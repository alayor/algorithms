import java.awt.Color;
import edu.princeton.cs.algs4.Picture;

public class SeamCarverTest {

    public static void runTests() {
        testPicture();
        testHeight();
        testWidth();
        testEnergy();
        testFindVerticalSeam();
        testFindHorizontalSeam();
        testRemoveVerticalSeam();
        testRemoveHorizontalSeam();
    }

    public static void testPicture() {
        Picture picture = new Picture(200, 200);
        SeamCarver seamCarver = new SeamCarver(picture);
        assert seamCarver.picture() != null;
    }

    public static void testHeight() {
        Picture picture = new Picture(200, 300);
        SeamCarver seamCarver = new SeamCarver(picture);
        assert 300 == seamCarver.height();
    }

    public static void testWidth() {
        Picture picture = new Picture(200, 300);
        SeamCarver seamCarver = new SeamCarver(picture);
        assert 200 == seamCarver.width();
    }

    public static void testEnergy() {
        Picture picture = createPicture();
        SeamCarver seamCarver = new SeamCarver(picture);
        assert 228.08770243044668 == seamCarver.energy(1, 2);
        assert 228.52789764052878 == seamCarver.energy(1, 1);
        assert 1000.0 == seamCarver.energy(0, 0);
        assert 1000.0 == seamCarver.energy(0, 1);
        assert 1000.0 == seamCarver.energy(picture.width() - 1, 0);
        assert 1000.0 == seamCarver.energy(0, picture.height() - 1);
        assert 1000.0 == seamCarver.energy(picture.width() - 1, picture.height() - 1);
    }

    private static Picture createPicture() {
        Picture picture = new Picture(3, 4);
        picture.set(0, 0, new Color(255, 101, 51));
        picture.set(0, 1, new Color(255, 153, 51));
        picture.set(0, 2, new Color(255, 203, 51));
        picture.set(0, 3, new Color(255, 255, 51));
        picture.set(1, 0, new Color(255, 101, 153));
        picture.set(1, 1, new Color(255, 153, 153));
        picture.set(1, 2, new Color(255, 204, 153));
        picture.set(1, 3, new Color(255, 255, 153));
        picture.set(2, 0, new Color(255, 101, 255));
        picture.set(2, 1, new Color(255, 153, 255));
        picture.set(2, 2, new Color(255, 205, 255));
        picture.set(2, 3, new Color(255, 255, 255));
        return picture;
    }

    public static void testFindVerticalSeam() {
        Picture picture = createPicture();
        SeamCarver seamCarver = new SeamCarver(picture);
        int[] verticalSeam = seamCarver.findVerticalSeam();
        assert verticalSeam.length > 0 : verticalSeam;
    }

    public static void testFindHorizontalSeam() {
        Picture picture = createPicture();
        SeamCarver seamCarver = new SeamCarver(picture);
        int[] horizontalSeam = seamCarver.findHorizontalSeam();
        assert horizontalSeam.length > 0 : horizontalSeam;
    }

    public static void testRemoveVerticalSeam() {
        Picture picture = createPicture();
        SeamCarver seamCarver = new SeamCarver(picture);
        int[] verticalSeam = seamCarver.findVerticalSeam();
        assert seamCarver.width() == 3 : seamCarver.width();
        seamCarver.removeVerticalSeam(verticalSeam);
        assert seamCarver.width() == 2 : seamCarver.width();
    }


    public static void testRemoveHorizontalSeam() {
        Picture picture = createPicture();
        SeamCarver seamCarver = new SeamCarver(picture);
        int[] verticalSeam = seamCarver.findHorizontalSeam();
        assert seamCarver.height() == 4 : seamCarver.height();
        seamCarver.removeHorizontalSeam(verticalSeam);
        assert seamCarver.height() == 3 : seamCarver.height();
    }
}