import edu.princeton.cs.algs4.Picture;
import edu.princeton.cs.algs4.StdOut;

import java.util.*;

import java.awt.*;

public class SeamCarver {
    private Picture _picture;
    private double[][] energies;

    public SeamCarver(Picture picture) {
        _picture = picture;
        createEnergyArray();
    }

    private void createEnergyArray() {
        energies = new double[_picture.width()][_picture.height()];
        for (int j = 0; j < _picture.height(); j++) {
            for (int i = 0; i < _picture.width(); i++) {
                energies[i][j] = energy(i, j);
            }
        }
    }

    public Picture picture() {
        return _picture;
    }

    public int height() {
        return _picture.height();
    }

    public int width() {
        return _picture.width();
    }

    public double energy(int x, int y) {
        if (x == 0 || y == 0 || x == _picture.width() - 1 || y == _picture.height() - 1) {
            return 1000.0;
        }
        double deltaX = getDeltaX(x, y);
        double deltaY = getDeltaY(x, y);
        return Math.sqrt(deltaX + deltaY);
    }

    private double getDeltaX(int x, int y) {
        if (0 < x && x < _picture.width() - 1) {
            Color colorFront = _picture.get(x + 1, y);
            Color colorBack = _picture.get(x - 1, y);
            return getColorDifference(colorFront, colorBack);
        }
        return 0.0;
    }

    private double getColorDifference(Color colorFront, Color colorBack) {
        double deltaRed = Math.pow(colorFront.getRed() - colorBack.getRed(), 2);
        double deltaGreen = Math.pow(colorFront.getGreen() - colorBack.getGreen(), 2);
        double deltaBlue = Math.pow(colorFront.getBlue() - colorBack.getBlue(), 2);
        return deltaRed + deltaGreen + deltaBlue;
    }

    private double getDeltaY(int x, int y) {
        if (0 < y && y < _picture.height() - 1) {
            Color colorUp = _picture.get(x, y - 1);
            Color colorDown = _picture.get(x, y + 1);
            return getColorDifference(colorUp, colorDown);
        }
        return 0.0;
    }

    public int[] findVerticalSeam() {
        int[] seam = new int[_picture.height()];
        Random random = new Random();
        int col = random.nextInt(_picture.width());
        seam[0] = col;
        for (int i = 0; i < _picture.height() - 1; i++) {
            col = determineCol(col, i);
            seam[i + 1] = col;
        }
        return seam;
    }

    private int determineCol(int col, int i) {
        double energyLeft = getEnergyLeft(col, i);
        double energyCenter = getEnergyCenter(col, i);
        double energyRight = getEneryRight(col, i);
        if (energyLeft < Math.min(energyCenter, energyRight)) {
            return col - 1;
        } else if (energyRight < energyCenter) {
            return col + 1;
        } else {
            return col;
        }
    }

    private double getEnergyLeft(int col, int i) {
        return col == 0 ? Integer.MAX_VALUE : energies[col - 1][i + 1];
    }

    private double getEnergyCenter(int col, int i) {
        return energies[col][i + 1];
    }

    private double getEneryRight(int col, int i) {
        return col == _picture.width() - 1 ? Integer.MAX_VALUE : energies[col + 1][i + 1];
    }

    public int[] findHorizontalSeam() {
        transposeImage();
        int[] horizontalSteam = findVerticalSeam();
        transposeImage();
        return horizontalSteam;
    }

    private void transposeImage() {
        Picture transposedPicture = new Picture(_picture.height(), _picture.width());
        for (int col = 0; col < _picture.width(); col++) {
            for (int row = 0; row < _picture.height(); row++) {
                transposedPicture.set(row, col, _picture.get(col, row));
            }
        }
        _picture = transposedPicture;
        createEnergyArray();
    }

    private void printEnergyPicture() {
        for (int j = 0; j < _picture.height(); j++) {
            for (int i = 0; i < _picture.width(); i++)
                StdOut.printf("%9.0f ", energy(i, j));
            StdOut.println();
        }
    }

    public void removeVerticalSeam(int[] seam) {
        Picture newPicture = new Picture(_picture.width() - 1, _picture.height());
        for (int row = 0; row < _picture.height(); row++) {
            for(int col = 0, origCol = 0; col < newPicture.width(); col++, origCol++) {
                if(col == seam[row]) origCol++;
                newPicture.set(col, row, _picture.get(origCol, row));
            }
        }
        _picture = newPicture;
        createEnergyArray();
    }

    public void removeHorizontalSeam(int[] seam) {
        transposeImage();
        removeVerticalSeam(seam);
        transposeImage();
    }

    public static void main(String[] args) {
    }
}