import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;

import java.util.*;

public class BaseballElimination {

    private final List<String> _teamNames = new ArrayList<>();
    private final Map<String, Stats> _teamStats = new HashMap<>();

    public BaseballElimination(String filename) {
        fillTeamNames(filename);
        fillStats(filename);
    }

    private void fillTeamNames(String filename) {
        In in = new In(filename);
        in.readLine();
        while (in.hasNextLine()) {
            StringTokenizer stringTokenizer = new StringTokenizer(in.readLine());
            _teamNames.add(stringTokenizer.nextToken());
        }
    }

    private void fillStats(String filename) {
        In in = new In(filename);
        in.readLine();
        while (in.hasNextLine()) {
            readTeamStats(in);
        }
    }

    private void readTeamStats(In in) {
        StringTokenizer stringTokenizer = new StringTokenizer(in.readLine());
        String name = stringTokenizer.nextToken();
        Stats stats = createStats(stringTokenizer);
        addStats(name, stats);
    }

    private Stats createStats(StringTokenizer stringTokenizer) {
        int wins = Integer.parseInt(stringTokenizer.nextToken());
        int losses = Integer.parseInt(stringTokenizer.nextToken());
        int remaining = Integer.parseInt(stringTokenizer.nextToken());
        Map<String, Integer> remainingVs = getRemainingVs(stringTokenizer);
        return new Stats(wins, losses, remaining, remainingVs);
    }

    private Map<String, Integer> getRemainingVs(StringTokenizer stringTokenizer) {
        Map<String, Integer> remainingVs = new HashMap<>();
        int indexTeam = 0;
        while (stringTokenizer.hasMoreTokens()) {
            remainingVs.put(_teamNames.get(indexTeam), Integer.parseInt(stringTokenizer.nextToken()));
            indexTeam++;
        }
        return remainingVs;
    }

    private void addStats(String name, Stats stats) {
        _teamStats.put(name, stats);
    }

    public static void main(String[] args) {
        BaseballEliminationTest.runTests();
    }

    public int numberOfTeams() {
        return _teamNames.size();
    }

    public Iterable<String> teams() {
        return _teamStats.keySet();
    }

    public int wins(String teamName) {
        if (isInvalidTeam(teamName)) {
            throw new IllegalArgumentException();
        }
        return _teamStats.get(teamName).wins;
    }

    private boolean isInvalidTeam(String teamName) {
        return !_teamNames.contains(teamName);
    }

    public int losses(String teamName) {
        if (isInvalidTeam(teamName)) {
            throw new IllegalArgumentException();
        }
        return _teamStats.get(teamName).losses;
    }

    public int remaining(String teamName) {
        if (isInvalidTeam(teamName)) {
            throw new IllegalArgumentException();
        }
        return _teamStats.get(teamName).remaining;
    }

    public int against(String teamName1, String teamName2) {
        if (isInvalidTeam(teamName1) || isInvalidTeam(teamName2)) {
            throw new IllegalArgumentException();
        }
        return _teamStats.get(teamName1).remainingVs.get(teamName2);
    }

    public boolean isEliminated(String targetTeam) {
        if (isInvalidTeam(targetTeam)) {
            throw new IllegalArgumentException();
        }
        FlowNetworkCreator flowNetworkCreator = new FlowNetworkCreator();
        FlowNetwork flowNetwork = flowNetworkCreator.createFlowNetwork(numberOfTeams(), targetTeam);
        FordFulkerson fordFulkerson = new FordFulkerson(flowNetwork, 0, 1);
        Map<String, Integer> teamNodeIndices = flowNetworkCreator._teamNodeIndices;
        for (String team : teamNodeIndices.keySet()) {
            if (fordFulkerson.inCut(teamNodeIndices.get(team))) {
                return true;
            }
        }
        return false;
    }

    public Iterable<String> certificateOfElimination(String teamName) {
        if (isInvalidTeam(teamName)) {
            throw new IllegalArgumentException();
        }
        if (!isEliminated(teamName)) {
            return null;
        }
        List<String> otherTeams = new ArrayList<>();
        for (String otherTeam : _teamNames) {
            if (!teamName.equals(otherTeam)) {
                otherTeams.add(otherTeam);
            }
        }
        return otherTeams;
    }

    private class Stats {
        private final int wins;
        private final int losses;
        private final int remaining;
        private final Map<String, Integer> remainingVs = new HashMap<>();

        public Stats(int wins, int losses, int remaining, Map<String, Integer> remainingVs) {
            this.wins = wins;
            this.losses = losses;
            this.remaining = remaining;
            this.remainingVs.putAll(remainingVs);
        }
    }

    private class FlowNetworkCreator {
        private Map<String, Integer> _teamNodeIndices;
        private String _targetTeam;
        private Stats _targetTeamStats;
        private int _currentIndexForNodes;

        public FlowNetwork createFlowNetwork(int numberOfTeams, String targetTeam) {
            _teamNodeIndices = new HashMap<>();
            _currentIndexForNodes = 2;
            _targetTeam = targetTeam;
            _targetTeamStats = _teamStats.get(_targetTeam);
            final int allOtherTeams = numberOfTeams - 1;
            return getFlowNetwork(allOtherTeams);
        }

        private FlowNetwork getFlowNetwork(int allOtherTeams) {
            FlowNetwork flowNetwork = new FlowNetwork(2 + allOtherTeams + getTriangleNumber(allOtherTeams - 1));
            List<FlowEdge> flowEdges = new ArrayList<>();
            flowEdges.addAll(createFlowEdgesForTeams());
            flowEdges.addAll(createFlowEdgesRemainingGames());
            addAllFlowEdges(flowNetwork, flowEdges);
            return flowNetwork;
        }

        private void addAllFlowEdges(FlowNetwork flowNetwork, List<FlowEdge> flowEdges) {
            for (FlowEdge flowEdge : flowEdges) {
                flowNetwork.addEdge(flowEdge);
            }
        }

        private int getTriangleNumber(int n) {
            int triangleNumber = 0;
            for (int i = 1; i <= n; i++) {
                triangleNumber += i;
            }
            return triangleNumber;
        }

        private List<FlowEdge> createFlowEdgesForTeams() {
            List<FlowEdge> flowEdges = new ArrayList<>();

            for (String currentTeam : _teamNames) {
                if (!currentTeam.equals(_targetTeam)) {
                    addFlowEdgesForTeams(flowEdges, currentTeam);
                }
            }
            return flowEdges;
        }

        private void addFlowEdgesForTeams(List<FlowEdge> flowEdges, String currentTeam) {
            Stats currentTeamStats = _teamStats.get(currentTeam);
            int numberOfGamesCanStillWin = Math.max(_targetTeamStats.wins + _targetTeamStats.remaining - currentTeamStats.wins, 0);
            flowEdges.add(new FlowEdge(_currentIndexForNodes, 1, numberOfGamesCanStillWin));
            _teamNodeIndices.put(currentTeam, _currentIndexForNodes);
            _currentIndexForNodes++;
        }

        private List<FlowEdge> createFlowEdgesRemainingGames() {
            List<FlowEdge> flowEdges = new ArrayList<>();
            Map<TeamGameNode, Integer> teamGameIndices = new HashMap<>();

            for (String team : _teamNodeIndices.keySet()) {
                final int teamIndex = _teamNames.indexOf(team);

                Set<Map.Entry<String, Integer>> remainingGames = _teamStats.get(team).remainingVs.entrySet();
                for (Map.Entry<String, Integer> remaining : remainingGames) {
                    if(remaining.getKey().equals(_targetTeam) || remaining.getKey().equals(team)) continue;

                    int teamVsIndex = _teamNames.indexOf(remaining.getKey());
                    TeamGameNode teamGameNode = new TeamGameNode(teamIndex, teamVsIndex);
                    Integer index = teamGameIndices.get(teamGameNode);
                    if (index == null) {
                        index = _currentIndexForNodes++;
                        teamGameIndices.put(teamGameNode, index);
                        flowEdges.add(new FlowEdge(0, index, remaining.getValue()));
                    }
                    flowEdges.add(new FlowEdge(index, _teamNodeIndices.get(team), Double.POSITIVE_INFINITY));
                }
            }
            return flowEdges;
        }

        private class TeamGameNode {
            private int index1;
            private int index2;

            public TeamGameNode(int index1, int index2) {
                this.index1 = index1;
                this.index2 = index2;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                TeamGameNode that = (TeamGameNode) o;

                return (index1 == that.index1 && index2 == that.index2) ||
                        index1 == that.index2 && index2 == that.index1;

            }

            @Override
            public int hashCode() {
                return 31 + index1 + index2;
            }
        }
    }
}
