import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BaseballEliminationTest {
    private static BaseballElimination baseballElimination = new BaseballElimination("test-files/teams4.txt");

    public static void runTests() {
        testNumberOfTeams();
        testTeams();
        testWins();
        testLosses();
        testRemaining();
        testAgainstAtlanta();
        testAgainstPhiladelphia();
        testAgainstNew_York();
        testAgainstMontreal();
        testIsEliminated();
        testCertificateOfElimination();
    }

    private static void testNumberOfTeams() {
        assert baseballElimination.numberOfTeams() == 4;
    }

    private static void testTeams() {
        Iterable<String> teamNames = baseballElimination.teams();
        assert teamNames != null;
        Set<String> teams = new HashSet<>();
        for (String team : teamNames) {
            teams.add(team);
        }
        assert teams.contains("Atlanta");
        assert teams.contains("Philadelphia");
        assert teams.contains("New_York");
        assert teams.contains("Montreal");
    }

    private static void testWins() {
        assert baseballElimination.wins("Atlanta") == 83;
        assert baseballElimination.wins("Philadelphia") == 80;
        assert baseballElimination.wins("New_York") == 78;
        assert baseballElimination.wins("Montreal") == 77;
    }

    private static void testLosses() {
        assert baseballElimination.losses("Atlanta") == 71;
        assert baseballElimination.losses("Philadelphia") == 79;
        assert baseballElimination.losses("New_York") == 78;
        assert baseballElimination.losses("Montreal") == 82;
    }

    private static void testRemaining() {
        assert baseballElimination.remaining("Atlanta") == 8;
        assert baseballElimination.remaining("Philadelphia") == 3;
        assert baseballElimination.remaining("New_York") == 6;
        assert baseballElimination.remaining("Montreal") == 3;
    }

    private static void testAgainstAtlanta() {
        assert baseballElimination.against("Atlanta", "Atlanta") == 0;
        assert baseballElimination.against("Atlanta", "Philadelphia") == 1;
        assert baseballElimination.against("Atlanta", "New_York") == 6;
        assert baseballElimination.against("Atlanta", "Montreal") == 1;
    }

    private static void testAgainstPhiladelphia() {
        assert baseballElimination.against("Philadelphia", "Atlanta") == 1;
        assert baseballElimination.against("Philadelphia", "Philadelphia") == 0;
        assert baseballElimination.against("Philadelphia", "New_York") == 0;
        assert baseballElimination.against("Philadelphia", "Montreal") == 2;
    }

    private static void testAgainstNew_York() {
        assert baseballElimination.against("New_York", "Atlanta") == 6;
        assert baseballElimination.against("New_York", "Philadelphia") == 0;
        assert baseballElimination.against("New_York", "New_York") == 0;
        assert baseballElimination.against("New_York", "Montreal") == 0;
    }

    private static void testAgainstMontreal() {
        assert baseballElimination.against("Montreal", "Atlanta") == 1;
        assert baseballElimination.against("Montreal", "Philadelphia") == 2;
        assert baseballElimination.against("Montreal", "New_York") == 0;
        assert baseballElimination.against("Montreal", "Montreal") == 0;
    }

    private static void testIsEliminated() {
        assert baseballElimination.isEliminated("Philadelphia");
        assert !baseballElimination.isEliminated("New_York");

        BaseballElimination baseballElimination1 = new BaseballElimination("test-files/teams4a.txt");
        assert baseballElimination1.isEliminated("Ghaddafi");

        BaseballElimination baseballElimination2 = new BaseballElimination("test-files/teams5.txt");
        assert baseballElimination2.isEliminated("Detroit");
    }

    private static void testCertificateOfElimination() {
        Iterable<String> teams = baseballElimination.certificateOfElimination("Philadelphia");
        List<String> eliminatingTeams = new ArrayList<>();
        for (String team : teams) {
            eliminatingTeams.add(team);
        }
        assert eliminatingTeams.contains("Atlanta");
        assert eliminatingTeams.contains("New_York");
        assert eliminatingTeams.contains("Montreal");
    }
}